<?php

/**
 * @file
 * Form functions and definitions
 */

function qr_redirect_add_form($form, &$form_state, $qrr) {

  $qrr_form = variable_get('qrr_form', FALSE);

  $form['qr_id'] = array(
    '#type' => 'hidden',
    '#value' => $qrr->qr_id
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter absolute URL with http://www.your_url.com/...'),
    '#description' => t('Like: http://www.your_url.com/product/84228932/index.html'),
    '#size' => 60,
    '#default_value' => (!empty($qrr->url)) ? $qrr->url : 'http://',
    '#maxlength' => 256,
    '#required' => TRUE,
    '#attributes' => array('id' => array('url')),
  );

  $form['hash_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the base url for the hash url'),
    '#description' => t('(optional) Enter base url. If empty the base URL will be used.'),
    '#size' => 60,
    '#default_value' => (!empty($qrr->hash_base_url)) ? $qrr->hash_base_url : $qrr_form['qrr_base_url'],
    '#maxlength' => 256,
    '#required' => FALSE,
    '#attributes' => array('id' => array('url')),
    '#disabled' => $qrr_form['qrr_endis_base_urls'],
  );

  $form['qr_desc'] = array(
    '#title' => t('Enter description'),
    '#type' => 'textarea',
    '#default_value' => $qrr->qr_desc,
    '#description' => t('Description for the QRcode link'),
    '#attributes' => array('id' => array('description'))
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Input QRcode'),
  );

  field_attach_form('qrr', $qrr, $form, $form_state);

  return $form;
}


function qr_redirect_add_form_validate($form, &$form_state) {
  $qrr_submisttion = (object) $form_state['values'];
  field_attach_form_validate('qrr', $qrr_submisttion, $form, $form_state);
}


function qr_redirect_add_form_submit($form, &$form_state) {

  // If we have the form, is an array and if there is no qr code id refference, create the qr code.
  // Else only the target url and the description will be changed.

  if (isset($form) && is_array($form) && empty($form['qr_id']['#value'])) {
    module_load_include('inc', 'qr_redirect', 'inc/qrcode_codec');
    $form_state['values']['hash_base_url'] = ( !empty($form_state['values']['hash_base_url']) ) ? $form_state['values']['hash_base_url'] : $GLOBALS['base_url'] . '/link/';
    $form_state['values'] += _gen_qrcode_img($form_state['values']['hash_base_url']);
  }


  $qrr_submission = (object)$form_state['values'];

  field_attach_submit('qrr', $qrr_submission, $form, $form_state);

  if (qr_redirect_save( $qrr_submission ) == SAVED_NEW || qr_redirect_save( $qrr_submission ) == SAVED_UPDATED) {
        $form_state['redirect'] = '/admin/config/qrr/manage';
  }
  else {
    watchdog('QRcode redirect', 'Failed to save: %qr_id failed.', array('%qr_id' => $qrr->qr_id), WATCHDOG_ERROR);
    drupal_set_message(t('Failed to save changes at QRcode redirect entity.'), 'error');
    return (FALSE);
  }

}


/**
 * Menu callback -- ask for confirmation of QRcode deletion
 */

function qr_redirect_delete_confirm($form, &$form_state, $qrr) {
  $form['#qrr'] = $qrr;
  $form['qr_id'] = array('#type' => 'value', '#value' => $qrr->qr_id);

  return confirm_form(
    $form,
    t('Are you sure you want to delete QRcode with id: %qr_id URL: !url?, Description: %desc', array( '%qr_id' => $qrr->qr_id, '!url' => l($qrr->hash_url, $qrr->url, array('attributes' => array('target' => array('_blank')))), '%desc' => $qrr->qr_desc )),
    '/admin/config/qrr/manage',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes QRcode deletion
 */

function qr_redirect_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {

    entity_get_controller('qrr') -> delete(array($form_state['values']['qr_id']));

    watchdog('content', 'Deleted QRcode with ID %qr_id', array('%qr_id' => $form_state['values']['qr_id'] ));
    drupal_set_message(t('%qr_id has been deleted.', array('%qr_id' => $form_state['values']['qr_id'])));
  }

  $form_state['redirect'] = '/admin/config/qrr/manage';
}


/**
 * Executes QRcode saving
 */

function qr_redirect_save(&$qrr) {
  return entity_get_controller('qrr')->save($qrr);
}


/**
 * Provides the QR code setting form
 */

function qr_redirect_admin_settings() {

  $qrr_form = variable_get('qrr_form', array());
  $qrr_code = variable_get('qrr_code', array());

  $form['qrr_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('QR code add form settings'),
    '#tree' => TRUE
  );

  $form['qrr_form']['qrr_endis_base_urls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable base url Input field editing'),
    '#default_value' => $qrr_form['qrr_endis_base_urls'],
    '#description' => t('If enabled, the QR code base url will be able to edit. Otherwise, the actual system URL will be used.'),
  );

  $form['qrr_form']['qrr_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Preset the hasch base URL Address'),
    '#size' => 60,
    '#default_value' => $qrr_form['qrr_base_url'],
    '#description' => t('This URL will be in the input field for the Base url.')
  );

  $form['qrr_code'] = array(
    '#type' => 'fieldset',
    '#title' => t('QR code image setting'),
    '#tree' => TRUE
  );

  $form['qrr_code']['qrr_secret_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Select code used at hashing of the QR code link'),
    '#size' => 60,
    '#default_value' => $qrr_code['qrr_secret_code'],
    '#description' => t('hash code = md5( time() . $secret_code )')
  );

  $form['qrr_code']['qrr_small_img_size'] = array(
    '#type' => 'select',
    '#title' => t('Set the size of the thumbnail (preview) QR code image size'),
    '#default_value' => $qrr_code['qrr_small_img_size'],
    '#options' => array(
      '1' => '1 - ' . t('smallest size'),
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7- ' . t('medium size')
      ),
  );

  $form['qrr_code']['qrr_big_img_size'] = array(
    '#type' => 'select',
    '#title' => t('Set the size of the big QR code image size'),
    '#default_value' => $qrr_code['qrr_big_img_size'],
    '#options' => array(
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7- ' . t('medium size'),
      '8' => '8',
      '9' => '9',
      '10' => '10',
      '11' => '11',
      '12' => '12 - ' . t('bigest size')
      ),
  );

  $form['qrr_code']['qrr_margin'] = array(
    '#type' => 'select',
    '#title' => t('Margin size'),
    '#default_value' => $qrr_code['qrr_margin'],
    '#options' => array(
      '0' => '0 -' . t('no border'),
      '1' => '1 px',
      '2' => '2 px',
      '3' => '3 px',
      '4' => '4 px',
      '5' => '5 px',
      '10' => '10 px',
      '20' => '20 px'
    )
  );

  $form['qrr_code']['qrr_level'] = array(
    '#type' => 'select',
    '#title' => t('Coding level'),
    '#default_value' => $qrr_code['qrr_level'],
    '#options' => array(
      '0' => '0',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      'L' => 'L',
      'M' => 'M',
      'Q' => 'Q',
      'H' => 'H'
    )
  );

  return system_settings_form($form);
}