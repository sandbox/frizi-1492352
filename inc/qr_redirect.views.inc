<?php

/**
 * @file
 * Provides the QR code fields for views
 * Defines the default view for QR codes
 */

/**
 * Implements hook_views_data().
 *
 * @todo Add joins to other entity tables. There are no joins to other tables
 * because we would have to make sure the entity type is respected.
 */


function qr_redirect_views_data() {

  /* default fields start */
  $string_field = array(
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $numeric_field = array('sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_field_numeric',
    ),
  );

  /* default fields end */

  $data['qr_redirect']['table']['group'] = t('QRcode redirect');

  $data['qr_redirect']['table']['base'] = array(
    'field' => 'qr_id',
    'title' => t('QRcode Redirect'),
    'help' => t('QRcode content.')
  );
  $data['qr_redirect']['table']['join'] = array(
    'node' => array(
      'left_field' => 'uid',
      'field' => 'qr_uid',
    ),
  );

  $data['qr_redirect']['qr_id'] = array(
    'title' => t('QRcode ID'),
    'help' => t('The QRcode content id.'),
  );
  $data['qr_redirect']['qr_id'] += $numeric_field;

  $data['qr_redirect']['url'] = array(
    'title' => t('QRcode Target URL'),
    'help' => t('The target url of the QRcode.'),
  );
  $data['qr_redirect']['url'] += $string_field;

  $data['qr_redirect']['hash_base_url'] = array(
    'title' => t('QRcode hash base URL'),
    'help' => t('The base page URL of the hashed url.'),
  );
  $data['qr_redirect']['hash_base_url'] += $string_field;

  $data['qr_redirect']['hash_url'] = array(
    'title' => t('QRcode hash URL'),
    'help' => t('The hash part of url.'),
  );
  $data['qr_redirect']['hash_url'] += $string_field;

  $data['qr_redirect']['qr_desc'] = array(
    'title' => t('QRcode description'),
    'help' => t('The QRcode description.'),
  );
  $data['qr_redirect']['qr_desc'] += $string_field;

  $data['qr_redirect']['qr_uid'] = array(
    'title' => t('QRcode User ID'),
    'help' => t('The user ID of the user who created it.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('QRcode user id'),
    ),
  );
  $data['qr_redirect']['qr_uid'] += $numeric_field;

  $data['qr_redirect']['qr_created'] = array(
    'title' => t('QRcode Creation date'),
    'help' => t('The timestamp of QRcode creation.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}


/**
 * Implements hook_views_default_views().
 */

function qr_redirect_views_default_views() {

  $view = new view;
  $view->name = 'vp_qrcode_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'qr_redirect';
  $view->human_name = 'VP QRcode list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'QRcode Ansicht';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'qr_id' => 'qr_id',
    'qr_id_1' => 'qr_id_1',
    'hash_base_url' => 'hash_base_url',
    'hash_url' => 'hash_url',
    'url' => 'url',
    'qr_desc' => 'qr_desc',
    'qr_created' => 'qr_created',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'qr_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'qr_id_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hash_base_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hash_url' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'qr_desc' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'qr_created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: QRcode redirect: QRcode ID */
  $handler->display->display_options['fields']['qr_id']['id'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_id']['field'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id']['label'] = 'ID';
  $handler->display->display_options['fields']['qr_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['text'] = ' <div style="width:80px;">[qr_id]</div>';
  $handler->display->display_options['fields']['qr_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_id']['element_type'] = 'div';
  $handler->display->display_options['fields']['qr_id']['element_class'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['qr_id']['format_plural'] = 0;
  /* Field: QRcode redirect: QRcode ID */
  $handler->display->display_options['fields']['qr_id_1']['id'] = 'qr_id_1';
  $handler->display->display_options['fields']['qr_id_1']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_id_1']['field'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id_1']['label'] = 'Image';
  $handler->display->display_options['fields']['qr_id_1']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['alter']['text'] = '<a href="/qrr/gen_img/[qr_id_1]_1" target="_blank"><img src="/qrr/gen_img/[qr_id_1]_0" alt="QRcode image" /></a>';
  $handler->display->display_options['fields']['qr_id_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_id_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['qr_id_1']['format_plural'] = 0;
  /* Field: QRcode redirect: QRcode hash base URL */
  $handler->display->display_options['fields']['hash_base_url']['id'] = 'hash_base_url';
  $handler->display->display_options['fields']['hash_base_url']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['hash_base_url']['field'] = 'hash_base_url';
  $handler->display->display_options['fields']['hash_base_url']['label'] = 'Hash URL';
  $handler->display->display_options['fields']['hash_base_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['hash_base_url']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['hash_base_url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['hash_base_url']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['hash_base_url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['hash_base_url']['hide_alter_empty'] = 1;
  /* Field: QRcode redirect: QRcode hash URL */
  $handler->display->display_options['fields']['hash_url']['id'] = 'hash_url';
  $handler->display->display_options['fields']['hash_url']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['hash_url']['field'] = 'hash_url';
  $handler->display->display_options['fields']['hash_url']['label'] = 'Hash URL';
  $handler->display->display_options['fields']['hash_url']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['hash_url']['alter']['text'] = '<a href="[hash_base_url]/link/[hash_url]" target="_blank">[hash_url]</a>';
  $handler->display->display_options['fields']['hash_url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['hash_url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['hash_url']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['hash_url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['hash_url']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['hash_url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['hash_url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['hash_url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['hash_url']['hide_alter_empty'] = 1;
  /* Field: QRcode redirect: QRcode Target URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['label'] = 'Target URL';
  $handler->display->display_options['fields']['url']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['url']['alter']['text'] = '<a href="[url]" target="_blank">External link</a>';
  $handler->display->display_options['fields']['url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['url']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['url']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['url']['hide_alter_empty'] = 1;
  /* Field: QRcode redirect: QRcode description */
  $handler->display->display_options['fields']['qr_desc']['id'] = 'qr_desc';
  $handler->display->display_options['fields']['qr_desc']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_desc']['field'] = 'qr_desc';
  $handler->display->display_options['fields']['qr_desc']['label'] = 'Description';
  $handler->display->display_options['fields']['qr_desc']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_desc']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_desc']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_desc']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_desc']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_desc']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_desc']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_desc']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_desc']['hide_alter_empty'] = 1;
  /* Field: QRcode redirect: QRcode Creation date */
  $handler->display->display_options['fields']['qr_created']['id'] = 'qr_created';
  $handler->display->display_options['fields']['qr_created']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_created']['field'] = 'qr_created';
  $handler->display->display_options['fields']['qr_created']['label'] = 'Creation date';
  $handler->display->display_options['fields']['qr_created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_created']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['qr_created']['date_format'] = 'medium';
  /* Field: QRcode redirect: QRcode ID */
  $handler->display->display_options['fields']['qr_id_2']['id'] = 'qr_id_2';
  $handler->display->display_options['fields']['qr_id_2']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_id_2']['field'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id_2']['label'] = 'Edit';
  $handler->display->display_options['fields']['qr_id_2']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['alter']['text'] = 'edit';
  $handler->display->display_options['fields']['qr_id_2']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['alter']['path'] = 'qrr/[qr_id_2]/edit';
  $handler->display->display_options['fields']['qr_id_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['element_type'] = 'div';
  $handler->display->display_options['fields']['qr_id_2']['element_class'] = 'qr_edit';
  $handler->display->display_options['fields']['qr_id_2']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_id_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['qr_id_2']['format_plural'] = 0;
  /* Field: QRcode redirect: QRcode ID */
  $handler->display->display_options['fields']['qr_id_3']['id'] = 'qr_id_3';
  $handler->display->display_options['fields']['qr_id_3']['table'] = 'qr_redirect';
  $handler->display->display_options['fields']['qr_id_3']['field'] = 'qr_id';
  $handler->display->display_options['fields']['qr_id_3']['label'] = 'Delete';
  $handler->display->display_options['fields']['qr_id_3']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['alter']['text'] = 'delete';
  $handler->display->display_options['fields']['qr_id_3']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['alter']['path'] = 'qrr/[qr_id_3]/delete';
  $handler->display->display_options['fields']['qr_id_3']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['external'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['alter']['html'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['element_type'] = 'div';
  $handler->display->display_options['fields']['qr_id_3']['element_class'] = 'qr_delete';
  $handler->display->display_options['fields']['qr_id_3']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['hide_empty'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['empty_zero'] = 0;
  $handler->display->display_options['fields']['qr_id_3']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['qr_id_3']['format_plural'] = 0;
  /* Sort criterion: QRcode redirect: QRcode Creation date */
  $handler->display->display_options['sorts']['qr_created']['id'] = 'qr_created';
  $handler->display->display_options['sorts']['qr_created']['table'] = 'qr_redirect';
  $handler->display->display_options['sorts']['qr_created']['field'] = 'qr_created';
  $handler->display->display_options['sorts']['qr_created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['qr_created']['granularity'] = 'minute';
  /* Sort criterion: QRcode redirect: QRcode ID */
  $handler->display->display_options['sorts']['qr_id']['id'] = 'qr_id';
  $handler->display->display_options['sorts']['qr_id']['table'] = 'qr_redirect';
  $handler->display->display_options['sorts']['qr_id']['field'] = 'qr_id';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'OR',
  );
  /* Filter criterion: QRcode redirect: QRcode hash URL */
  $handler->display->display_options['filters']['hash_url']['id'] = 'hash_url';
  $handler->display->display_options['filters']['hash_url']['table'] = 'qr_redirect';
  $handler->display->display_options['filters']['hash_url']['field'] = 'hash_url';
  $handler->display->display_options['filters']['hash_url']['operator'] = 'contains';
  $handler->display->display_options['filters']['hash_url']['group'] = 1;
  $handler->display->display_options['filters']['hash_url']['exposed'] = TRUE;
  $handler->display->display_options['filters']['hash_url']['expose']['operator_id'] = 'hash_url_op';
  $handler->display->display_options['filters']['hash_url']['expose']['label'] = 'Search upon Hash URL';
  $handler->display->display_options['filters']['hash_url']['expose']['operator'] = 'hash_url_op';
  $handler->display->display_options['filters']['hash_url']['expose']['identifier'] = 'hash_url';
  $handler->display->display_options['filters']['hash_url']['expose']['required'] = 0;
  $handler->display->display_options['filters']['hash_url']['expose']['multiple'] = FALSE;
  /* Filter criterion: QRcode redirect: QRcode description */
  $handler->display->display_options['filters']['qr_desc']['id'] = 'qr_desc';
  $handler->display->display_options['filters']['qr_desc']['table'] = 'qr_redirect';
  $handler->display->display_options['filters']['qr_desc']['field'] = 'qr_desc';
  $handler->display->display_options['filters']['qr_desc']['operator'] = 'contains';
  $handler->display->display_options['filters']['qr_desc']['group'] = 1;
  $handler->display->display_options['filters']['qr_desc']['exposed'] = TRUE;
  $handler->display->display_options['filters']['qr_desc']['expose']['operator_id'] = 'qr_desc_op';
  $handler->display->display_options['filters']['qr_desc']['expose']['label'] = 'Search upon Description';
  $handler->display->display_options['filters']['qr_desc']['expose']['operator'] = 'qr_desc_op';
  $handler->display->display_options['filters']['qr_desc']['expose']['identifier'] = 'qr_desc';
  $handler->display->display_options['filters']['qr_desc']['expose']['required'] = 0;
  $handler->display->display_options['filters']['qr_desc']['expose']['multiple'] = FALSE;
  $translatables['vp_qrcode_list'] = array(
    t('Master'),
    t('QRcode Ansicht'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('ID'),
    t('<div style="width:80px;">[qr_id]</div>'),
    t('.'),
    t(','),
    t('Image'),
    t('<a href="/qrr/gen_img/[qr_id_1]_1" target="_blank"><img src="/qrr/gen_img/[qr_id_1]_0" alt="QRcode image" /></a>'),
    t('Hash URL'),
    t('<a href="[hash_base_url]/link/[hash_url]" target="_blank">[hash_url]</a>'),
    t('Target URL'),
    t('<a href="[url]" target="_blank">External link</a>'),
    t('Description'),
    t('Creation date'),
    t('Edit'),
    t('edit'),
    t('qrr/[qr_id_2]/edit'),
    t('Delete'),
    t('delete'),
    t('qrr/[qr_id_3]/delete'),
    t('Search upon Hash URL'),
    t('Search upon Description'),
  );
  $views[$view->name] = $view;
  return $views;
}


