<?php

/**
 * @file
 * QR code image generation functions and tools
 */

function _gen_qrcode_img($base_hash_url = FALSE) {
    global $user;

    $qrr_code = variable_get('qrr_code', FALSE);

    $ret_arr = array();
    $path = file_directory_temp();

    $img_small_path = $path . 'QRimage.png';
    $img_big_path = $path . 'QRimage_big.png';
    $ret_arr['qr_created'] = time();
    $ret_arr['qr_uid'] = $user->uid;

    module_load_include('php', 'qr_redirect', 'lib/qrlib');

    $ret_arr['hash_url'] = md5($ret_arr['qr_created'] . $qrr_code['qrr_secret_code']);

    // extermal static class
    // public static function png($text, $outfile = false, $level = QR_ECLEVEL_L, $size = 3, $margin = 4, $saveandprint=false)
    // level 0-3, L, M, Q, H ~ compression
    // size 1-12 ~ cm
    // margin around

    QRcode::png($base_hash_url . $ret_arr['hash_url'], $img_small_path, $qrr_code['qrr_level'], $qrr_code['qrr_small_img_size'], $qrr_code['qrr_margin'], FALSE);
    QRcode::png($base_hash_url . $ret_arr['hash_url'], $img_big_path, $qrr_code['qrr_level'], $qrr_code['qrr_big_img_size'], $qrr_code['qrr_margin'], FALSE);

    if (file_exists($img_small_path) && file_exists($img_big_path)) {

    // fopen the temporary files to get the binary content
    $handle = fopen($img_small_path, "rb");
    $ret_arr['qr_image'] = base64_encode(fread($handle, filesize($img_small_path)));
    fclose($handle);

    $handle = fopen($img_big_path, "rb");
    $ret_arr['qr_image_big'] = base64_encode(fread($handle, filesize($img_big_path)));
    fclose($handle);

    // remove temporary files
        unlink($img_small_path);
        unlink($img_big_path);
    }
    return ($ret_arr);
}